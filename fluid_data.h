#ifndef FLUID_DATA_GUARD
#define FLUID_DATA_GUARD

#include <vector>
#include <string>

// would it be better to have the grid be a return value? using a pointer or something?
void load_pgm_to_grid(std::string file_name, std::vector<std::vector<float>> &grid);

void write_grid_to_pgm(std::string file_name, std::vector<std::vector<float>> const &grid);

void print_grid_as_pgm(const std::vector<std::vector<float>> &grid);

void fill_grid_random(std::vector<std::vector<float>> &grid);

// true if both grids have the same dimensions
bool compare_grid_dimensions(std::vector<std::vector<float>> const &grid1,
                             std::vector<std::vector<float>> const &grid2);

#endif // FLUID_DATA_GUARD