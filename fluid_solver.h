#ifndef FLUID_SOLVER_GUARD
#define FLUID_SOLVER_GUARD

#include <vector>

void density_step(std::vector<std::vector<float>> *unknown,
                  std::vector<std::vector<float>> *source,
                  std::vector<std::vector<float>> const &velocity_x,
                  std::vector<std::vector<float>> const &velocity_y,
                  float diffusion_rate,
                  float delta_time);

void velocity_step(std::vector<std::vector<float>>* unknown_velocity_x,
                   std::vector<std::vector<float>>* unknown_velocity_y,
                   std::vector<std::vector<float>>* source_velocity_x,
                   std::vector<std::vector<float>>* source_velocity_y,
                   float viscosity,
                   float delta_time);

#endif // FLUID_SOLVER_GUARD