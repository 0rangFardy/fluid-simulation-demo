#ifndef DRAW_FLUID_GUARD
#define DRAW_FLUID_GUARD

#include <vector>

void draw_densities(std::vector<std::vector<float>> const &density);

void draw_velocities(std::vector<std::vector<float>> const &velocity_x,
                     std::vector<std::vector<float>> const &velocity_y);

#endif