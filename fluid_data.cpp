#include "fluid_data.h"
#include "constants.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>
#include <random>

#define PGM_PATH "pgms/"

#pragma region PGM I/O 

// Roughly using Plain PGM Spec from http://netpbm.sourceforge.net/doc/pgm.html

// load the numbers stored in a pgm to a 2d vector of floats
// if the vector is not empty, empty before loading
void load_pgm_to_grid(std::string file_name, std::vector<std::vector<float>> &grid)
{
    std::ifstream pgm_in(PGM_PATH+file_name);

    if (!pgm_in.is_open())
    {
        std::cout << "function load_pgm_to_grid failed to open file\n";
        return;
    }

    if (grid.size() != 0)
    {
        std::cout << "WARNING: vector already has elements, emptying before load...\n";
        grid.empty();
    }

    // read magic number
    std::string magic_number; pgm_in >> magic_number;
    if (magic_number != "P2"){
        std::cout << "ERROR: Magic Number 'P2' not present\n" ;
    }

    // read grid dimensions
    int width; pgm_in >> width;
    int height; pgm_in >> height;

    // read maxvalue; TODO?: standardise maxvalues between the pgms and the program
    float maxval; pgm_in >> maxval;
    if (maxval != MAX_DENSITY_VALUE) {
        std::cout << "WARNING: pgm defined MaxValue is different to MAX_DENSITY_VALUE\n";
    }

    //read grid values
    for (size_t i = 0; i < height; i++)
    {
        std::vector<float> row;
        for (size_t j = 0; j < width; j++)
        {
            float grid_val; pgm_in >> grid_val;
            
            
            if(grid_val > maxval){
                std::cout << "WARNING: value at " << i << " " << j << "exceeds max value\n";
            }

            row.push_back(grid_val);
        }
        grid.push_back(row);
    }

    // check for any errors reading before closing
    if (pgm_in.fail())
    {
        std::cout << "failure in reading pgm file: "
                  << pgm_in.rdstate();
        return;
    }
    pgm_in.close();
}

void write_grid_to_pgm(std::string file_name, const std::vector<std::vector<float>> &grid)
{
    std::ofstream pgm_out(PGM_PATH+file_name);

    if (!pgm_out.is_open()){
        std::cout << "Error opening file\n";
        return;
    }

    pgm_out << "P2\n"; // write magic number

    // write width and height to first line
    size_t width = grid.at(0).size();
    size_t height = grid.size();
    pgm_out << width << ' ' << height << '\n';

    pgm_out << MAX_DENSITY_VALUE << '\n'; // write program defined Maxval

    // write grid values
    for (std::vector<float> row : grid)
    {
        for (float float_val : row)
        {
            if (float_val >= 100000.0f)
            {
                std::cout << "warning: size of float exceeds formatting width\n";
            }
            // use io formatting manipulators to align floats in columns
            //pgm_out << std::setw(6) << std::setprecision(3)
            pgm_out << std::fixed << std::setw(10) << std::setprecision(5)
                    << float_val << ' ';

            if (!pgm_out.good())
            {
                std::cout << "error writing to file";
            }
        }
        pgm_out << '\n';
    }

    pgm_out.close();
}

void print_grid_as_pgm(const std::vector<std::vector<float>> &grid)
{
    size_t width = grid.at(0).size();
    size_t height = grid.size();
    std::cout << "DIMENSIONS: " << width << ' ' << height << '\n';

    for (std::vector<float> row : grid)
    {
        for (float float_val : row)
        {
            if (float_val >= 100000.0f)
            {
                std::cout << "warning: size of float exceeds formatting width\n";
            }

            std::cout << std::fixed << std::setw(10) << std::setprecision(5)
                      << float_val << ' ';
        }
        std::cout << '\n';
    }
    
}

#pragma endregion

// fill a grid with random integers ranging from 0 to MAX_DENSITY_VALUE
void fill_grid_random(std::vector<std::vector<float>> &grid){ 
    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(1, MAX_DENSITY_VALUE);

    for (std::vector<float> &row : grid){
        for (float &val : row) {
            val = distribution(generator);
        }
        
    }
}

// compare the parent vector sizes and the sizes of all vector elements
// to be absolutely sure dimensions are the same
bool compare_grid_dimensions(std::vector<std::vector<float>> const &grid1,
                             std::vector<std::vector<float>> const &grid2)
{

    if (grid1.size() != grid2.size())
    {
        return false;
    }

    for (size_t i = 0; i < grid1.size(); i++)
    {

        if(grid1.at(i).size() != grid2.at(i).size()){
            return false;
        }
    }
    return true;
}
