#include "fluid_data.cpp"

// build with g++ -std=c++11 -o test_fluid_data test_fluid_data.cpp

// test the load function by iterating through the 2d std::vector
// and printing the floats
void print_pgm_after_load(std::string file_name)
{
    std::vector<std::vector<float>> test_grid;
    load_pgm_to_grid(file_name, test_grid);

    std::cout << "width: " << test_grid.at(0).size() << '\n';
    std::cout << "height: " << test_grid.size() << '\n';

    //print pgm back to console
    for (std::vector<float> row : test_grid)
    {
        for (float grid_val : row)
        {
            std::cout << grid_val << ' ';
        }
        std::cout << '\n';
    }
}


// produce a pgm generated from a 2d vector
// for visual inspection
void test_write_grid_to_pgm()
{
    std::vector<std::vector<float>> test_grid;

    for (int i = 0; i < 10; i++)
    {
        std::vector<float> row;
        for (int j = 0; j < 10; j++)
        {
            // some arithmetic to give a different float for each element
            row.push_back((float)i + 1.0f / (j + 1));
        }
        test_grid.push_back(row);
    }
    
    write_grid_to_pgm("test_write.pgm", test_grid);
}


// read a pgm to a 2d vector
// make some arithmetic manipulations to vector
// write to another pgm
// load this to another 2d vector and compare with first vector
void test_read_write_pgm(std::string file_name)
{
    std::vector<std::vector<float>> test_grid;
    std::vector<float> garbage = {41678.9, 12.33, 1234.33}; // throw some garbage values into the vector to check load function overwrites
    load_pgm_to_grid(file_name, test_grid);

    // make some changes to the pgm now that it is loaded
    for (std::vector<float> row : test_grid)
    {
        for (float grid_val : row)
        {
            grid_val += 1;
            grid_val *= 0.5f;
        }
    }

    // write this new 
    write_grid_to_pgm("feep_loaded_serialized.pgm", test_grid);

    std::vector<std::vector<float>> compare_grid;
    load_pgm_to_grid("feep_loaded_serialized.pgm", compare_grid);

    // check dimensions and individual values are the same when reading back
    if (!compare_grid_dimensions(test_grid, compare_grid)){
        std::cout << "test failed: dimensions of grid are"
                     "different after writing and loading back";
        return;
    }
    size_t row_size = test_grid.at(0).size();
    for (size_t i = 0; i < test_grid.size(); i++)
    {

        if (test_grid[i].size() != row_size)
        {
            std::cout << "note: inconsistent row sizes";
        }

        for (size_t j = 0; j < test_grid[i].size(); j++)
        {
            if (test_grid[i][j] != compare_grid[i][j])
            {
                std::cout << "failed comparison at " << i << " " << j << '\n';
                std::cout << "grid val before writing:  " << test_grid[i][j] << '\n';
                std::cout << "grid val after reloading: " << compare_grid[i][j] << '\n';
                return;
            }
        }
    }

    std::cout << "tests for read/write pgm to 2d vector passed\n";
}

// filling a zero intitialised grid with random numbers and printing for inspection
void test_fill_grid_random(){
    std::cout << "test_fill_grid_random\n"; 
    
    std::vector<std::vector<float>> grid (10, std::vector<float> (10, 0));

    std::cout << "\nBefore filling with random vals\n";
    print_grid_as_pgm(grid);

    fill_grid_random(grid);

    std::cout << "\nAfter filling with random vals\n";
    print_grid_as_pgm(grid);
}

int main(int argc, char const *argv[])
{
    // print_pgm_after_load("concentrated_centre.pgm");
    // print_pgm_after_load("unit_uniform_field.pgm");

    //test_write_grid_to_pgm();

    // test_read_write_pgm("feep_float.pgm");

    test_fill_grid_random();
    
    // test_fill_grid_random();
    
    return 0;
}
