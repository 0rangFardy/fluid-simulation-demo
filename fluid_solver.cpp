
// This is the fluid solver presented in Jos Stam's paper Real Time Fluid Dynamics for Games (2003)
// ...as well as his book The Art of Fluid Animation (2016)
//
// It has been refactored for convenience of use and readability using C++11

// It is assumed that all 2D vectors (grids) passed to this solver are of the same size

#include "fluid_solver.h"
#include "fluid_data.h"
#include <iostream>

// #define SWAP(ptr1, ptr2) std::vector<std::vector<float>>* tmp = ptr1;\
//                                                           ptr1 = ptr2;\
//                                                           ptr2 = tmp;

// add the source grid to the fluid grid
// sources can come from user mouse clicking, for example
void add_source(
    std::vector<std::vector<float>> &grid,
    std::vector<std::vector<float>> const &source_grid,
    float delta_time)
{
    for (size_t i = 0; i < grid.size(); i++)
    {
        for (size_t j = 0; j < grid[i].size(); j++)
        {
            grid[i][j] += delta_time * source_grid[i][j];
        }
    }
}

// boundary modes:
// 0: set val to neighbour cell (for density)
// 1: set horizontal boundary val to opposite of neighbour cell (for vertical velocity)
// 2: set vertical boundary val to opposite of neighbour cell (for horizontal velocity)
void set_boundaries(std::vector<std::vector<float>> &grid, int mode)
{
    size_t width = grid.at(0).size();
    size_t height = grid.size();

    // horizontal boundaries
    for (size_t i = 0; i < width; i++)
    {
        grid[0][i] = (mode == 1 ? -grid[1][i]
                                : grid[1][i]);
        grid[height - 1][i] = (mode == 1 ? -grid[height - 2][i]
                                         : grid[height - 2][i]);
    }
    // vertical boundaries
    for (size_t i = 0; i < height; i++)
    {
        grid[i][0] = (mode == 2 ? -grid[i][1]
                                : grid[i][1]);
        grid[i][width - 1] = (mode == 2 ? -grid[i][width - 2]
                                        : grid[i][width - 2]);
    }

    // set corner boundaries to average of neighbour boundaries
    grid[0][0] = 0.5 * grid[0][1] * grid[1][0];
    grid[0][width - 1] = 0.5 * grid[0][width - 2] * grid[1][width - 1];
    grid[height - 1][0] = 0.5 * grid[height - 1][1] * grid[height - 2][0];
    grid[height - 1][width - 1] = 0.5 * grid[height - 1][width - 1] * grid[height - 2][width - 1];
}

// generic gauss-seidel solver for use in diffusion and projection functions
// a and c are to be adjusted for different solvers to produce a stable result
void linear_solve(std::vector<std::vector<float>> &unknown,
                  std::vector<std::vector<float>> const &known,
                  float a,
                  float c,
                  int boundary_mode)
{
    for (int iteration = 0; iteration < 20; iteration++)
    {
        for (size_t i = 1; i < unknown.size() - 1; i++)
        {
            for (size_t j = 1; j < unknown.at(0).size() - 1; j++)
            {
                unknown[i][j] = (known[i][j] +
                                 a * (known[i - 1][j] +
                                      known[i + 1][j] +
                                      known[i][j - 1] +
                                      known[i][j + 1])
                                ) / c;
            }
        }
    }

    set_boundaries(unknown, boundary_mode);
}

// use the generic linear solver to diffuse grid vals
// with a constant involving a custom diffusion rate and the grid size 
void diffuse(std::vector<std::vector<float>> &unknown,
             std::vector<std::vector<float>> const &known,
             float diffusion_constant,
             float delta_time,
             int boundary_mode)
{
    float a = delta_time * diffusion_constant * unknown.size() * unknown.at(0).size();
    linear_solve(unknown, known, a, 1+4*a, boundary_mode);
}

// transport the cell values along the vector field
// by following the vector backwards 
// assumes all grids are of same dimensions
void advect(std::vector<std::vector<float>> &unknown,
            std::vector<std::vector<float>> const &known,
            std::vector<std::vector<float>> const &velocity_x,
            std::vector<std::vector<float>> const &velocity_y,
            float delta_time,
            int boundary_mode
            )
{   
    // set the distance that we will follow the velocity backwards
    // scaling this value to the dimensions of the grid should give similar behaviours for different grid size
    // might make more sense to take the square root of the product?
    float dt0 = delta_time * ((unknown.size() + unknown.at(0).size()) / 2);    

    for (size_t i = 1; i < unknown.size() - 1; i++)
    {
        for (size_t j = 1; j < unknown.at(0).size() - 1; j++)
        {
            // find the previous position of an imagined particle at the centre of
            // each grid cell using our velocity field
            float x = i - dt0 * velocity_x[i][j];
            float y = j - dt0 * velocity_y[i][j];

            // clamp positions to borders if outside grid
            if (x < 0.5f) { x = 0.5f; } 
            if (x > unknown.at(0).size() - 0.5f ) { x = unknown.size() - 0.5f; }
            if (y < 0.5f) { y = 0.5f; }
            if (y > unknown.size() - 0.5f) { y = unknown.size() - 0.5f; }
            
            // get the cell borders that the particle is enclosed in 
            // by rounding up and down 
            int i0 = (int)x; int i1 = i0 + 1;
            int j0 = (int)y; int j1 = j0 + 1;

            // get the positions in the grid cell as percentages for interpolation
            float s1 = x - i0; float s0 = 1 - s1; float t1 = y - j0; float t0 = 1 - t1;

            // interpolate grid values 
            unknown[i][j] = s0 * (t0 * known[i0][j0] + t1 * known[i0][j1]) + 
                            s1 * (t0 * known[i1][j0] + t1 * known[i1][j1]); 
        }
    }

    set_boundaries(unknown, boundary_mode);
}

// make the fluid imcompressible iteratively solving the poisson equation
// assumed all grids have the same dimensions
void project(std::vector<std::vector<float>> &velocity_x,
             std::vector<std::vector<float>> &velocity_y,
             std::vector<std::vector<float>> &projection,
             std::vector<std::vector<float>> &divergence)
{
    // compute divergence at each cell 
    // and zero initialise projection grid
    for (size_t i = 1; i < projection.size() - 1; i++)
    {
        for (size_t j = 1; j < projection.at(0).size() - 1; j++)
        {
            divergence[i][j] = -0.5f * (velocity_x[i+1][j] - velocity_x[i-1][j] +
                                        velocity_y[i][j-1] - velocity_y[i][j-1]);
            projection[i][j] = 0;
        }
    }

    set_boundaries(divergence, 0); 
    set_boundaries(projection, 0);
    
    // use the linear solver to solve the poisson equation
    // this will give us a projection of the grid we can use to 
    // make the fluid incompressible
    linear_solve(projection, divergence, 1, 4, 0);

    // subtract the gradient of the pressure from the velocity field  
    for (size_t i = 1; i < projection.size() - 1; i++)
    {
        for (size_t j = 1; j < projection.at(0).size() - 1; j++)
        {
            velocity_x[i][j] -= 0.5f * (projection[i+1][j] - projection[i-1][j]);
            velocity_y[i][j] -= 0.5f * (projection[i][j+1] - projection[i][i-1]);
        }
    }
    set_boundaries(velocity_x, 1);
    set_boundaries(velocity_y, 2);
}

void density_step(std::vector<std::vector<float>> *unknown,
                  std::vector<std::vector<float>> *source,
                  std::vector<std::vector<float>> const &velocity_x,
                  std::vector<std::vector<float>> const &velocity_y,
                  float diffusion_rate,
                  float delta_time)
{
    add_source(*unknown, *source, delta_time);

    // use the now unused source grid for the new purpose of
    // keeping the previous grid value
    std::vector<std::vector<float>>* previous_grid = source;

    // Swapping pointers before diffuse and advect
    // because each time we get a new "previous_grid" and
    // and the old one now has useless data, so it can be used for our unknown
    
    std::swap(previous_grid, unknown);
    diffuse(*unknown, *previous_grid, diffusion_rate, delta_time, 0);

    std::swap(previous_grid, unknown);
    advect(*unknown, *previous_grid, velocity_x, velocity_y, delta_time, 0);
}

void velocity_step(std::vector<std::vector<float>>* unknown_velocity_x,
                   std::vector<std::vector<float>>* unknown_velocity_y,
                   std::vector<std::vector<float>>* source_velocity_x,
                   std::vector<std::vector<float>>* source_velocity_y,
                   float viscosity,
                   float delta_time)
{
    add_source(*unknown_velocity_x, *source_velocity_x, delta_time);
    add_source(*unknown_velocity_y, *source_velocity_y, delta_time);

    // rename the source grids to use their memory for our linear solver
    std::vector<std::vector<float>>* prev_velocity_x = source_velocity_x;
    std::vector<std::vector<float>>* prev_velocity_y = source_velocity_y;

    // swap vector pointers each time we use the linear solver
    std::swap(prev_velocity_x, unknown_velocity_x); 
    diffuse(*unknown_velocity_x, *prev_velocity_x, viscosity, delta_time, 1);
    std::swap(prev_velocity_y, unknown_velocity_y);
    diffuse(*unknown_velocity_y, *prev_velocity_y, viscosity, delta_time, 2);

    // make the fluid incompressible. the 3rd and 4th grids here are just used for their memory
    project(*unknown_velocity_x, *unknown_velocity_y, *prev_velocity_x, *prev_velocity_y);

    // move the velocities along the velocity field, then make the fluid incompressible again
    std::swap(prev_velocity_x, unknown_velocity_x);
    std::swap(prev_velocity_y, unknown_velocity_y);
    advect(*unknown_velocity_x, *prev_velocity_x, *prev_velocity_x, *prev_velocity_y, delta_time, 1);
    advect(*unknown_velocity_y, *prev_velocity_y, *prev_velocity_x, *prev_velocity_y, delta_time, 2);
    project(*unknown_velocity_x, *unknown_velocity_y, *prev_velocity_x, *prev_velocity_y);
}


