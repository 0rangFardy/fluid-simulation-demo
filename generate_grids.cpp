#include "fluid_data.h"
#include "constants.h"

#include <vector>
#include <iostream>
#include <string>

// build with g++ -std=c++11 -o generate_grids generate_grids.cpp fluid_data.cpp 

// TODO?: Specify 

// generate a central blob in an otherwise 0 density grid
void gen_concentrated_centre(size_t size, size_t centre_size, float centre_cell_val){
    std::vector<std::vector<float>> grid(size, std::vector<float>(size, 0));

    if (centre_size >= size){
        std::cout << "ERROR: size of centre is greater than size of grid\n";
    }
 
    size_t centre_offset = size/2 - centre_size/2;
    for (size_t i = 0; i < centre_size; i++)
    {
        for (size_t j = 0; j < centre_size; j++)
        {
            grid[centre_offset + i][centre_offset + j] = centre_cell_val;
        }
    }

    write_grid_to_pgm("gen_concentrated_centre.pgm",grid);
    
    
}

//specify a size and a val, generated size*size grid will have all cells set to that val 
void gen_uniform_field(size_t size, float cell_val){
    std::vector<std::vector<float>> grid(size, std::vector<float>(size, cell_val));
    write_grid_to_pgm("gen_uniform_field.pgm", grid); 
}


int main(int argc, char const *argv[])
{
    gen_concentrated_centre(500, 100, 50);
    gen_uniform_field(500, 1);
    
    return 0;
}
