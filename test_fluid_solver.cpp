#include "fluid_solver.cpp" // test the unexposed functions as well
#include "fluid_data.h"

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

// build with g++ -std=c++11 -o test_fluid_solver test_fluid_solver.cpp fluid_data.cpp

#pragma region test fluid_solver

// see if we can get a blob in the centre of the grid to diffuse outwards
void test_diffuse(){
    std::cout << "\ntest diffuse\n";
    // initialise data 
    std::vector<std::vector<float>> initial;
    load_pgm_to_grid("concentrated_centre.pgm", initial);
    std::vector<std::vector<float>> unknown;
    
    // zero initalise a vector to output the diffusion calculations to 
    for (std::vector<float> row : initial){
        std::vector<float> arb_row;
        for (float val : row){
            arb_row.push_back(0);
        }
        unknown.push_back(arb_row);
    }

    diffuse(unknown, initial, 1, 5, 0);
    print_grid_as_pgm(unknown);
    
}

// test one step of diffusion followed by one step of advection, both applied to the density
void test_density_step(){
    std::cout << "\ntest density step\n";
    std::vector<std::vector<float>> initial;
    load_pgm_to_grid("concentrated_centre.pgm", initial);
    std::vector<std::vector<float>> unknown;

    // zero initalise a vector to output the diffusion calculations to 
    for (std::vector<float> row : initial){
        std::vector<float> arb_row;
        for (float val : row){
            arb_row.push_back(1);
        }
        unknown.push_back(arb_row);
    }

    // initialise some kind of uniform vector field for the advection 
    std::vector<std::vector<float>> velocity_x;
    std::vector<std::vector<float>> velocity_y;
    load_pgm_to_grid("unit_uniform_field.pgm", velocity_x);
    load_pgm_to_grid("unit_uniform_field.pgm", velocity_y);

    density_step(&unknown, &initial, velocity_x, velocity_y, 1, 0.001);
    print_grid_as_pgm(unknown);
}

void print_steps_from_initial_pgm(int num_steps, 
                                  float delta_time_fixed,
                                  float diffusion,
                                  float vicosity,
                                  std::string density_file,
                                  std::string velocity_x_file,
                                  std::string velocity_y_file
                                 )
{
    std::cout << "called print_steps.. \n";

    // load from initial pgms the density and two velocity grids
    std::vector<std::vector<float>> density;
    std::vector<std::vector<float>> velocity_x;
    std::vector<std::vector<float>> velocity_y;
    load_pgm_to_grid(density_file, density);
    load_pgm_to_grid(velocity_x_file, velocity_x);
    load_pgm_to_grid(velocity_y_file, velocity_y);

    std::cout << "\nINITIAL VALUES\n";
    std::cout << "DENSITY\n";    print_grid_as_pgm(density);
    std::cout << "VELOCITY X\n"; print_grid_as_pgm(velocity_x);
    std::cout << "VELOCITY Y\n"; print_grid_as_pgm(velocity_y);
    
    std::cout << "loaded pgms\n";

    // make sure all grids are the same size
    if (!compare_grid_dimensions(density, velocity_x) 
     || !compare_grid_dimensions(velocity_y, density)){
         std::cout << "files inputted are not of the same dimensions\n";
    }


    size_t width = density.size();
    size_t height = density.at(0).size();


    // create empty source grids by zero initialising  
    std::vector<std::vector<float>> prev_density   (height, std::vector<float>(width, 0));
    std::vector<std::vector<float>> prev_velocity_x(height, std::vector<float>(width, 0));
    std::vector<std::vector<float>> prev_velocity_y(height, std::vector<float>(width, 0));

    for (int i = 0; i < num_steps; i++)
    {
        // run the solver for one step with simple values for viscocity, diffusion and delta_time

        density_step(&density, &prev_density, velocity_x, velocity_y, diffusion, delta_time_fixed);
        velocity_step(&velocity_x, &velocity_y, &prev_velocity_x, &prev_velocity_y, vicosity, delta_time_fixed);

        // print out all three grids after a step through the solver
        std::cout << "\n--- STEP NUMBER " << i << " ---\n"; 
        std::cout << "***DENSITY****\n";    print_grid_as_pgm(density);
        std::cout << "***VELOCITY_X****\n"; print_grid_as_pgm(velocity_x);
        std::cout << "***VELOCITY_Y****\n"; print_grid_as_pgm(velocity_y);
        std::cout << "\n\n\n";
    }
}

// Run the fluid solver with the given parameters, saving a pgm of the density each step
void save_density_steps_pgm(int num_steps, 
                            float delta_time_fixed,
                            float diffusion,
                            float vicosity,
                            std::string density_file,
                            std::string velocity_x_file,
                            std::string velocity_y_file){
    // load from initial pgms the density and two velocity grids
    std::vector<std::vector<float>> density;
    std::vector<std::vector<float>> velocity_x;
    std::vector<std::vector<float>> velocity_y;
    load_pgm_to_grid(density_file, density);
    load_pgm_to_grid(velocity_x_file, velocity_x);
    load_pgm_to_grid(velocity_y_file, velocity_y);

    std::cout << "\nINITIAL VALUES\n";
    std::cout << "DENSITY\n";    print_grid_as_pgm(density);
    std::cout << "VELOCITY X\n"; print_grid_as_pgm(velocity_x);
    std::cout << "VELOCITY Y\n"; print_grid_as_pgm(velocity_y);
    
    std::cout << "loaded pgms\n";

    if (!compare_grid_dimensions(density, velocity_x) 
     || !compare_grid_dimensions(velocity_y, density)){
         std::cout << "files inputted are not of the same dimensions\n";
    }

    size_t width = density.size();
    size_t height = density.at(0).size();


    // create empty source grids by zero initialising  
    std::vector<std::vector<float>> prev_density   (height, std::vector<float>(width, 0));
    std::vector<std::vector<float>> prev_velocity_x(height, std::vector<float>(width, 0));
    std::vector<std::vector<float>> prev_velocity_y(height, std::vector<float>(width, 0));

    for (int i = 0; i < num_steps; i++)
    {
        // run the solver for one step with simple values for viscocity, diffusion and delta_time

        density_step(&density, &prev_density, velocity_x, velocity_y, diffusion, delta_time_fixed);
        velocity_step(&velocity_x, &velocity_y, &prev_velocity_x, &prev_velocity_y, vicosity, delta_time_fixed);

        // print out all three grids after a step through the solver
        std::cout << "\n--- STEP NUMBER " << i << " ---\n"; 
        std::cout << "***DENSITY****\n";    print_grid_as_pgm(density);
        std::cout << "***VELOCITY_X****\n"; print_grid_as_pgm(velocity_x);
        std::cout << "***VELOCITY_Y****\n"; print_grid_as_pgm(velocity_y);
        std::cout << "\n\n\n";

        std::stringstream ss; ss << "solverdens-step" << i ;
        std::string filename = ss.str();
        write_grid_to_pgm(filename, density);
    }
}


// call any function from the above tests
int main(int argc, char const *argv[])
{
    // define the number of steps, a fixed delta time, a diffusion constant and viscosity constant to see some test steps printed to console
    // print_steps_from_initial_pgm(3, 0.1, 0.01, 0.01,
    //     "concentrated_centre.pgm", "unit_uniform_field.pgm", "unit_uniform_field.pgm"
    //     );

    save_density_steps_pgm(3, 0.1, 0.01, 0.01,
        "gen_concentrated_centre.pgm", "unit_uniform_field.pgm", "unit_uniform_field.pgm");

    

    // test_diffuse();
    
    // test_density_step();

    return 0;
}

